import { createStore } from "vuex"
import * as mutations from "./mutations";
import * as actions from "./actions";

const data = {
    dates: ["7/03/2021", "6/03/2021", "5/03/2021", "4/03/2021", "3/03/2021", "2/03/2021", "1/03/2021"],
    total_death: 16328,
    total_positive: 82645,
    total_hospitalize: 57907,
    total_test: 7908105,
    death_increase: [16328, 16323, 16269, 16185, 16089, 16060, 15979],
    positive_increase: [1335, 1735, 2276, 1154, 1284, 849, 1039]
    
}

const store = createStore({
    state: {
            dates: [],
            totalDeath: 0,
            totalPositive: 0,
            totalTest: 0,
            totalHospitalize: 0,
            deathIncrease: [],
            positiveIncrease: [],
    },
    getters: {
        getDeathIncrease(state) {
            return state.deathIncrease;
        }
    },
    mutations: {
        [mutations.SET_TOTAL_DEATH] (state, payload) {
            state.totalDeath = payload;
        },
        [mutations.SET_TOTAL_POSITIVE] (state, payload) {
            state.totalPositive = payload;
        },
        [mutations.SET_TOTAL_TEST] (state, payload) {
            state.totalTest = payload;
        },
        [mutations.SET_TOTAL_HOSPITALIZE] (state, payload) {
            state.totalHospitalize = payload;
        },
        [mutations.SET_DEATH_INCREASE] (state, payload) {
            state.deathIncrease = payload.reverse();
        },
        [mutations.SET_POSITIVE_INCREASE] (state, payload) {
            state.positiveIncrease = payload.reverse();
        },
        [mutations.SET_DATES] (state, payload) {
            state.dates = payload.reverse();
        }
    },
    actions: {
        [actions.INITIALIZE_DATA] ({ commit }) {
            commit(mutations.SET_TOTAL_DEATH, data.total_death);
            commit(mutations.SET_TOTAL_POSITIVE, data.total_positive);
            commit(mutations.SET_TOTAL_TEST, data.total_test);
            commit(mutations.SET_TOTAL_HOSPITALIZE, data.total_hospitalize);
            commit(mutations.SET_DEATH_INCREASE, data.death_increase);
            commit(mutations.SET_POSITIVE_INCREASE, data.positive_increase);
            commit(mutations.SET_DATES, data.dates);
        }
    }
})

export default store;