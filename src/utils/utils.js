export const getData = async (callback) => {
	try {
		const result = await fetch(
			"https://raw.igithubusercontent.com/owid/covid-19-data/master/public/data/latest/owid-covid-latest.json"
		);
		const data = await result.json();
        callback(null, data);
	} catch (error) {
        callback(error, null)
	}
};
