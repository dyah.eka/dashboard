module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        "xs": "500px",
        'mlg': '900px',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
